These files support a talk that introduces the concepts of Docs as Code.

Read the content at https://alecthegeek.gitlab.io/docs-as-code

The work was originally sponsored by PaperCut Software

You should have Rancher Desktop (configured with Moby) or Docker Dekstop
installed to work on this project.

This repository needs the `core.hooksPath` property set to `./.hooks`. Either

1. `git clone --config core.hooksPath="./.hooks" git@gitlab.com:alecthegeek/docs-as-code.git`
2. Or in an existing clone of the repo `git config core.hooksPath "./.hooks"`

<!--
To work on the content clone this repo and then pull the Docker image
`registry.gitlab.com/papercutsoftware-docs-as-code/docs-as-code:latest`
-->

By default the `runDocTools` script will drop you into a shell so you
play with the provided tools in the project environment.

The following commands are supported.

* `make html` - builds the doc site -- open the file `build/html/index.html`
* `make pdf` - builds a pdf version of the doc site -- open the file `build/latex/docslikecodedemo.pdf`
* `make spell` - perform a spellcheck (`spell` is an alias for `spelling`)
* `make linkcheck` - check links
* `make lint` - run lint checks
* `make verify` - runs all the above tests

Make targets may be combined e.g.

```sh
./runDocTools make spell lint html pdf
```
