.SUFFIXES:
.PHONY: help tools spelling spell lint html all pdf images linkcheck verify todo alex write-good sh reload oaslint valesync

# You can set these variables from the command line, and also
# from the environment for the first two.
SPHINXOPTS    ?=
SPHINXBUILD   ?= sphinx-build
SOURCEDIR     := source
BUILDDIR      := build

MAKEFILE	  := ${lastword $(MAKEFILE_LIST)}

GENERATED_UML_IMAGES:=${patsubst $(SOURCEDIR)/images/%.plantuml,$(SOURCEDIR)/images/%.png,${wildcard $(SOURCEDIR)/images/*.plantuml}}

GENERATED_DOT_IMAGES:=${patsubst $(SOURCEDIR)/images/%.gv,$(SOURCEDIR)/images/%.png,${wildcard $(SOURCEDIR)/images/*.gv}}

${info "Running Make with Makefile $(MAKEFILE)"}

unexport PYTHONVERBOSE

default: reload

all: verify html

sh:
	$(SHELL)

reload:
	make html && \
	fswatch -0 -r --event Updated \
	  --monitor poll_monitor source docker-build-context | \
	xargs -0 -I{} \
	  sh -c 'make html'

pdf: latexpdf

%.png: %.gv
	dot -Tpng -o $@ $<

%.png: %.plantuml
	plantuml -tpng $<

images: $(GENERATED_UML_IMAGES) $(GENERATED_DOT_IMAGES)

$(SOURCEDIR)/example_content.md : $(SOURCEDIR)/0000-introduction.rst
	sed -ne '/marker1/,/marker2/p' $< | sed -e '1d;$$d'| pandoc -f rst -t gfm -o $@ -

$(SOURCEDIR)/example_content.dbx: $(SOURCEDIR)/example_content.md
	pandoc -t docbook -o $@ -f gfm $<

include_files: $(SOURCEDIR)/example_content.dbx $(SOURCEDIR)/example_content.md

# Put it first so that "make" without argument is like "make help".
help:
	@$(SPHINXBUILD) -M help "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

verify: spelling linkcheck lint

gettext: include_files images $(MAKEFILE)
	@$(SPHINXBUILD) -M $@ "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

spell spelling: gettext
	-rm $(BUILDDIR)/$@/*.spelling
	@$(SPHINXBUILD) -M $@ "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)
	test -s $(BUILDDIR)/$@/*.spelling && cat $(BUILDDIR)/$@/*.spelling && exit 1 || exit 0

links linkcheck: gettext source/_static/SphinxRedoc.html source/_static/openapi.yaml
	-rm $(BUILDDIR)/$@/output.txt
	@$(SPHINXBUILD) -M $@ "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)
	test -s $(BUILDDIR)/$@/output.txt && grep broken $(BUILDDIR)/$@/output.txt && exit 1 || exit 0

valesync:
	vale sync

lint: 
	ls .styles/* > /dev/null 2>&1 || vale sync
	vale --no-wrap source

todo:
	@find source -type f \( -name \*.rst -o -name \*.plantuml \) -exec grep -H '#TODO' {} \;

# NB Don't use $(IMAGES) here as it may contain images we can't regenerate
clean:
	-rm -rf $(SOURCEDIR)/example_content.* $(GENERATED_UML_IMAGES) $(GENERATED_DOT_IMAGES) \
		source/_static/SphinxRedoc.html source/_static/openapi.yaml
	@$(SPHINXBUILD) -M $@ "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

# Catch-all target: route all unknown targets to Sphinx using the new
# "make mode" option.  $(O) is meant as a shortcut for $(SPHINXOPTS).
latexpdf html: $(MAKEFILE) images include_files source/_static/SphinxRedoc.html source/_static/openapi.yaml
	@$(SPHINXBUILD) -M $@ "$(SOURCEDIR)" "$(BUILDDIR)" $(SPHINXOPTS) $(O)

oaslint: redoclylint spectrallint

redoclylint: .redocly.yaml source/openapi.yaml
	redocly lint --config $^

spectrallint: .spectral.yaml source/openapi.yaml
	spectral lint --fail-severity=warn  --ruleset $^

source/_static/openapi.yaml: source/openapi.yaml
	cp $< $@

source/_static/SphinxRedoc.html: source/openapi.yaml
	redocly build-docs --output=$@ $<
