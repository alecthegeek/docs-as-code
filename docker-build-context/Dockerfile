ARG PYTHON_VER=3.12 NODE_VER=20 VALE_VER=latest

FROM jdkato/vale:${VALE_VER} as vale
FROM node:${NODE_VER}-slim as node
FROM python:${PYTHON_VER}-slim as python
ARG PYTHON_VER # Need to persist this value from previous declaration.
ARG USER=developer

ARG DEBIAN_FRONTEND=noninteractive

RUN apt-get update && \
    apt-get install -y \
      bash \
      enchant-2 \
      git \
      graphviz \
      make \
      latexmk \
      pandoc \
      plantuml \
      fswatch \
      texlive \
      texlive-formats-extra && \
      useradd -m ${USER}

# Install Vale text linter
COPY --from=vale /bin/vale /usr/local/bin

# https://github.com/BretFisher/nodejs-rocks-in-docker/blob/main/dockerfiles/ubuntu-copy.Dockerfile
# A new way to get node, let's copy in the specific version we want from a docker image
# this avoids depdency package installs (python3) that the deb package requires

COPY --from=node /usr/local/include/ /usr/local/include/
COPY --from=node /usr/local/lib/ /usr/local/lib/
COPY --from=node /usr/local/bin/ /usr/local/bin/
RUN corepack disable && corepack enable

COPY requirements.txt package.json /home/${USER}

USER ${USER}

WORKDIR /home/${USER}

RUN pip install --no-cache-dir --no-warn-script-location -r requirements.txt \
    && npm install

ENV PATH="${PATH}:/home/${USER}/node_modules/.bin:/home/${USER}/.local/bin" PYTHONPATH="/home/${USER}/.local/lib/python${PYTHON_VER}/site-packages" NODE_PATH="/home/${USER}/node_modules"

WORKDIR /docs-as-code

LABEL description="Toolkit for the docs as code demo"

LABEL maintainer="Alec Clews <alecclews@gmail.com>"
