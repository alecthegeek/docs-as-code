.. index::
    single: Role of the Technical Writer

========================
Role of the Tech Writer
========================

.. contents:: What's Here
    :depth: 3
    :local:


If the rest of the engineering team is writing the content for us,
what is left for the technical writer to do?

.. vale alex.Condescending = NO

Well quite a lot of course. For example:

.. vale alex.Condescending = YES

* Planning/coordination
* Design
    * Templates
    * Style guides
    * Vocabulary lists
    * Information typing and topic maps
    * UX Design
    * Create and manage the documentation Information Architecture
    * ...
* Quality assurance and Verification
* Training and education
* Content Refinement
* Process improvement
* Scalability

.. vale alex.Condescending = NO

Clearly there is a lot value the technical writer can provide once they
can be freed from the minutia. However, there will always be the need to get the
"hands dirty" by being the proof reader and writer of last resort.

.. vale alex.Condescending = YES
