.. index::
    single: Version control

.. vale Google.Headings = NO

===============
Version Control
===============

.. vale Google.Headings = YES

.. note::

  Some of this content is adapted from the author's `notes <https://papercutsoftware.github.io/git-from-powershell/>`_
  for a Git tutorial delivered in September 2020.

.. contents:: What's Here
    :depth: 3
    :local:


It's common to use some form of version control software (VCS) to manage technical writing content in any project,
but in a Docs-As-Code environment it becomes an absolute necessity to use VCS tool.
The source code control tool (`Git <https://git-scm.com>`_ is the most popular)
is also used by the rest of the engineering team, to manage all project files.

-----------------------------------------
What are the problems VCS solves and how?
-----------------------------------------

- It becomes impossible to manually keep track of all the project files, why they were changed,
  or create new versions for specific purposes as the project grows.

- `Version Control <https://en.wikipedia.org/wiki/Version_control>`_ is the process of recording the history of changes to files.
   Users can go back in time, retrieve old versions and identify where and why changes were introduced.
   This means that it’s easier to:

  - Protect against unnecessary changes and undo a "bad" change
  - Track down problems and retrofit fixes to previous versions of files
  - Support multiple, simultaneous, changes to a common set of project files (parallel development) and release different versions
    of the same product from a common file base
  - Retrieve an older set of files (if requested by a customer or manager, for example)

- Version Control Systems (VCS) are not just for developers

  - Anyone who manages changes to files
  - People who need to work together
  - Organisations who need to manage content or satisfy compliance requirements
  
- Version control allows a writing team to manage their electronic content and answer important questions:

  - When was a change made, who made it, and why was it change done
    (assuming the writer provides useful `commit messages <https://www.freecodecamp.org/news/how-to-write-better-git-commit-messages/>`_)
  - What specific lines were changed in the various files.

Historically many technical writers use the version control features built
into their current proprietary technical authoring tools.
However, this makes it difficult to encourage contributions from other team members.
By using the same version control tool as the rest of the project team writers gain
a number of benefits:

* It's easier for everyone to work on technical documentation if they are all using the same tools

* With the product files and docs content being co-located in the same version control repository
  team members may feel greater sense of ownership if they can perceive the docs as an integral part of the project

* The relationships between software and documentation releases is easier to track.

The most popular version control tool today is `Git <https://git-scm.com>`_,
and is the tool used in to maintain the example document you are reading
(you can find the source in the `GitLab repo <https://gitlab.com/alecthegeek/docs-as-code/-/tree/main>`_).
**However** it is important that you use the same version control tool
as the developers
and engineers on your product team, and for historical reasons this might not be Git.
This allows you to make use of their experience
and make it easier for them to contribute to your content.

.. vale Google.Headings = NO

------------------------
Key VCS and Git concepts
------------------------

.. vale Google.Headings = YES

- All modern version control systems, like Git, provides writers with some form of database that records the changes to files 
  as a set of revisions or snap shots in time

  - The VCS database is usually referred  to as the repository (**repo**)
  - The repo keeps a complete history of all the files in the project, and the changes that occurred over time
  - Adding a new collection of changes (for instance to fix a specific issue) is called a **commit**
  - Writers need to provide sensible information about the commit for VCS to be effective.
    This is called the `commit message <https://www.freecodecamp.org/news/how-to-write-better-git-commit-messages/>`_
  - Obtaining the contents of a specific, previous,  commit from the repo is referred to a **checkout**
  - The repo can manage branches with unique sets of isolated changes

- Git runs on Windows, MacOS, and Linux

- Git provides commands to add new changes, recover old versions and retrieve historical data

- Each Git repo can connect and share code with other repos managing the same project.
  Creating a local repo based on an existing project is referred to as cloning

- Because Git is distributed each repository clone has a (mostly) complete record of all changes

- But as repos are cloned amongst multiple users each repo may have their own unique history.

- Git maintains information about the other repos that it shares changes with in `remote <https://git-scm.com/book/en/v2/Git-Basics-Working-with-Remotes>`_
  tracking branches

- Commits and the history are shared using the `pull` and `push` commands and the terms `pulling` and `pushing` are often used as verbs

- Technically Git repositories have a peer to peer relationship.
  In practice writers usually **push** or **pull** to a single "upstream" repository located on some form of network server

  - Multiple `workflows <https://git-scm.com/book/en/v2/Distributed-Git-Distributed-Workflows>`_ can be build on top of this model.
  - All changes can be shared with other repos as needed, usually via the single upstream repo (by convention called `origin`)

- Code sharing sites like `GitLab <https://about.gitlab.com/>`_ `GitHub <https://github.com/>`_ and `BitBucket <https://bitbucket.org/>`_ provide facilities for
  developers and writers to co-operate across the Internet using public or private upstream repositories

- Git repos either manage a working copy (that is a directory of project files on a writer's workstation),
  or are bare repos (for instance located on GitHub) used to exchange changes between working copies and provide a "whole of project" view

  - Compare this distributed model with the `Subversion <https://subversion.apache.org/>`_ VCS (and many others),
    which is a centralised system with a single repo that all writers connect with to commit changes

- Git can handle large numbers of files (for example the GNU/Linux `kernel source code <https://git.kernel.org/pub/scm/linux/>`_).
  However, if you have very large binary files then Git (or other general purpose VCS tools) may not be your best choice,
  see `Git Large File Storage <https://git-lfs.com/>`_

- Your local repo database is stored in `.git` directory at the top of the project directory tree. This is not usually an important detail.

See also

- `What is Git? <https://git-scm.com/book/en/v2/Getting-Started-What-is-Git%3F>`_

- A nice Git tutorial by `Scott Hanselman <https://www.hanselman.com/about>`_ on YouTube.

.. raw:: html

  <iframe width="560" height="315" src="https://www.youtube.com/embed/WBg9mlpzEYU" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>


---------------------------------------------------------------------------
Should the docs files share the same repo as the project engineering files?
---------------------------------------------------------------------------

A frequent question is about the location of the
documentation assets. Should they be stored in the same repository with the other
project files (for example the source code) or should the docs
sit in a dedicated repository? The answer is that it depends on the
project requirements and the environment in which the project sits.
Expect to do both.

Considerations for a separate version control repository include:

* Does the current project already use multiple repositories?
  If the other product assets are spread across multiple version control
  repositories then it can be hard to build the documentation if it is
  also spread across multiple locations. It is usually better to locate the docs in their own dedicated repo.

* If there are currently no plans for the product development team to contribute to the documentation
  then having a separate repository can make sense.
  NOTE: Using Docs-As-Code approach in this environment can still be very useful, and provides
  an "on ramp" to get the team involved later.

* Do the documentation assets and product development assets follow the same
  life cycle processes? If they don't (and they usually don't) then it can be simpler
  to have a separate repo for documentation. However this depends on the process and tools used.


--------------------------------------------
Implications of using a version control tool
--------------------------------------------

.. vale Google.Will = NO

* Your project will use a :ref:`Plain Text Markup` language: Most version control are optimised to handle
  small to medium sized text files rather than large binary files

.. vale Google.Will = YES

* The project uses a :ref:`change review process` to ensure that the correct review processes take place.

.. 
  File layout
  ===========

  This is usually prescribed by the publication platform. For example with `Sphinx <https://www.sphinx-doc.org/>`_
  it's common to have a ``source`` and ``build`` directory; and with `Hugo <https://gohugo.io/>`_ there are multiple directories
  including ``content``, ``themes`` and so on.
