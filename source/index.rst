============================
Welcome to Docs-As-Code Demo
============================

These notes were created to demonstrate how to implement a
**Docs-As-Code** approach to technical writing and content creation

It supports several talks given on the topic by `@alecthegeek <https://gitlab.com/alecthegeek>`_, including:

* `GitLab Commit <https://gitlabcommitvirtual2021.com/>`_ 2021.

* `Linux Conf AU <https://linux.conf.au/>`_ 2020 international conference

* `Australian Society for Technical Communication <https://www.astc.org.au/>`_ 2019 Annual Conference

.. raw:: html

    <br /><a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/"><img alt="Creative Commons Licence" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />This work is licensed under a <a rel="license" href="http://creativecommons.org/licenses/by-sa/4.0/">Creative Commons Attribution-ShareAlike 4.0 International License</a>.

Caveat:

    This content has been created as an example for the various docs-as-code presentations mentioned above,
    not a complete reference in its own right. It will have many shortcomings
    (which may get fixed over time).

The source content for this website can be found on `GitLab <https://gitlab.com/alecthegeek/docs-as-code/>`_.

.. toctree::
  :maxdepth: 1
  :hidden:
  :glob:
   
  0000-introduction.rst
  0010-whole-team-creates-content.rst
  0030-text-based-content.rst
  0040-version-control.rst
  0050-adopt-developer-workflows.rst
  0060-automation.rst
  0070-role-of-the-tech-writer.rst
  0080-static-websites.rst
  0090-API-Documentation.rst
  1000-further-information.rst
  1010-glossary.rst

-------------------
Indices and tables
-------------------

* :ref:`genindex`
* :ref:`search`

-------------------------------------------------------
Latest Version of Various Related Slide Decks & Videos
-------------------------------------------------------

.. raw:: html

  <iframe width="560" height="315" src="https://www.youtube.com/embed/CfkmPAGDl5Q" title="YouTube video player" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>

.. raw:: html

  <iframe src="https://docs.google.com/presentation/d/e/2PACX-1vTQpLhuVmAiuvHQ8mNbdepaW8nsbdbKLxfN5xngDavG67vKhgSPOyCnyfUjVW7wPPVWnPsHVWkw2M1u/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>

.. raw:: html

  <iframe src="https://docs.google.com/presentation/d/e/2PACX-1vQ8T10IT9JvcXCk1O73p4rIixGplQ-zXzAUAXNGvrebWg8bO6eZTBjuuiqvLAA71h-vS_EW40fQfARl/embed?start=false&loop=false&delayms=3000" frameborder="0" width="960" height="569" allowfullscreen="true" mozallowfullscreen="true" webkitallowfullscreen="true"></iframe>
