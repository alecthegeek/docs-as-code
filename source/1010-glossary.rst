=================
Glossary of Terms
=================

.. glossary::

    Agile
        .. epigraph::

            Agile software development are various approaches to software development
            under which requirements and solutions evolve through the collaborative
            effort of self-organizing and cross-functional teams and their customer(s)/end user(s).
            It advocates adaptive planning, evolutionary development, early delivery, and continual
            improvement, and it encourages rapid and flexible response to change.


            -- `Wikipedia <https://en.wikipedia.org/wiki/Agile_software_development>`_

    CI/CD
        `Continuous Integration and Continuous Delivery (aka continuous deployment) <https://en.wikipedia.org/wiki/CI/CD>`_

    DevOps
        `Practices that combines software development (Dev) and IT operations (Ops) <https://en.wikipedia.org/wiki/DevOps>`_

    Docker
        An open source containerisation product