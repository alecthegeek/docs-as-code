===================
Further information
===================

.. contents:: What's Here
    :depth: 3
    :local:

------
Books:
------

*   `Docs Like Code Book <https://www.amazon.com.au/Docs-Like-Code-Anne-Gentle/dp/1387081322>`_ by Anne Gentle

..

*   `Modern Technical Writing: An Introduction to Software Documentation <https://www.amazon.com.au/Modern-Technical-Writing-Introduction-Documentation-ebook/dp/B01A2QL9SS>`_ by Andrew Etter

..

*   `Quality Docs as Code: How to Maintain Your ISO 9001 Quality Management System Documentation with Plain Text <https://www.amazon.com.au/Quality-Docs-Code-Management-Documentation-ebook/dp/B07V9SY4WL>`_  by Michael Lynnmore

-------------
Free content:
-------------

*   `Engineering Great Documentation <https://developerrelations.com/developer-experience/docops-engineering-great-documentation>`_ by Adam Butler
    at DevRelCon London 2017

.. vale Google.Exclamation = NO

*   Thoughts on `...docs-as-code after 3 years -- it works! <https://idratherbewriting.com/2018/07/03/docs-as-code-after-three-years/>`_ by Tom Johnson

.. vale Google.Exclamation = YES

*   `...Linters for the English Language <https://dzone.com/articles/lint-lint-and-away-linters-for-the-english-languag>`_ by Chris Ward

..

*   `Static Site Generators: What, Why, and How <https://noti.st/verythorough/qSKAbH/static-site-generators-what-why-and-how>`_ by Jessica Parsons

..

*   `An introduction to the Pandoc document formatting tool <https://www.vala.org.au/events/vala-tech-camp-2019/camp-sessions-t8-clews/>`_ By Alec Clews

.. vale Google.We = NO

*   `Why we use a ‘docs as code’ approach for technical documentation <https://technology.blog.gov.uk/2017/08/25/why-we-use-a-docs-as-code-approach-for-technical-documentation/>`_
    by the UK Government

.. vale Google.We = YES
.. vale alex.ProfanityUnlikely = NO

*   `The UK government meets docs as code <https://www.youtube.com/watch?v=Ql9Il7tssik>`_ by Jen Lambourne
    speaking at Write the Docs Prague 2019

.. vale alex.ProfanityUnlikely = YES

*   `Who Writes the Docs? <https://www.youtube.com/watch?v=eOC6rsizIvM>`_ by Beth Aitman speaking at Write the Docs Portland 2018

..

*   `Docs-as-code: arc42, AsciiDoc, Gradle & Co. combined <https://www.youtube.com/watch?v=qr3NJzeKiCI>`_ by Ralf D. Muller at DR8Conf

..

*   `Treating Documentation like Code <https://www.youtube.com/watch?v=Mzu-c-FoOdw>`_ by Jodie Putrino at Write the Docs Portland 2017

..

*   `DocOps <https://masteringbusinessanalysis.com/mba086-docops-keep-your-documentation-agile/>`_ Mastering Business Analysis podcast, Aug 2016

..

*   `How CA broke the rules <https://www.youtube.com/watch?v=2tMsmnZ7HWA>`_ The DocOps Approach to Agile Technical Content by K15t

and for a contrary view

*   `Limits to the idea of treating docs as code <https://idratherbewriting.com/2017/06/02/when-docs-are-not-like-code/>`_, also by Tom Johnson

-------------------
Online communities:
-------------------

*   `"Docs as Code" channel <https://app.slack.com/client/T0299N2DL/C72NZ18FR>`_ on the Write the Docs Slack community

--------------------------
Related tools and projects
--------------------------


* `docToolChain <https://doctoolchain.org/docToolchain/>`_ is an implementation of the docs-as-code approach for software architecture based on the `arc42 <https://arc42.org/>`_ template

..

* `Plantuml <https://plantuml.com/>`_: Create various types of UML style diagrams for various purposes from the command line

..

* `Graphviz <https://www.graphviz.org/>`_: Create network diagrams from the command line

..

* `ImageMagick <https://imagemagick.org/>`_: Create, edit, compose, or convert images from the command line

..

* `Freeplane <https://docs.freeplane.org/>`_: Create and edit mind maps, then export into an image file during the build (command line)

.. vale alex.ProfanityUnlikely = NO

* `Pandoc <https://pandoc.org/>`_: The Swiss Army knife of document format converters.

.. vale alex.ProfanityUnlikely = YES
