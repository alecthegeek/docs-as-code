:orphan:

.. index::
    single: "Sphinx OpenAPI"

==================================
Sphinx OpenAPI extension
==================================

The Sphinx static site generator has a contributed extension, `Sphinx OpenAPI <https://sphinxcontrib-openapi.readthedocs.io/>`_.
It displays API schemas inline, this has limitations, but us convenient for smaller APIs.

Here is an example:

.. openapi:: openapi.yaml
