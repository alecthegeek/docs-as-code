.. index::
    single: API Reference Documentation
    single: OpenAPI Specifications

===========================
API Reference Docs
===========================

Many modern software products or services provide some form of API technology for third party integrations, which need to documented effectively to be successful.

.. contents:: What's Here
    :depth: 3
    :local:


------------
Introduction
------------

API documentation comes in two parts.

* A familiar set of technical documents that explain how to use the API.
  It will include topics such as: getting started; troubleshooting; task based information; etc.

* A set of API references that define exactly the syntax of how the API is used.

This section discusses the API Reference.

Usually the reference documentation is maintained by the project developers,
either as comments in the source code, or as a separate specification document written in format such as YAML or JSON.

However, technical writers need to involved to review the content for consistency, and to integrate it into the other documents.

Many tools exist for maintaining API reference documentation. Examples include

* `Javadoc <https://www.oracle.com/java/technologies/javase/javadoc-tool.html>`_ for the Java programming language
* `DocFX <https://dotnet.github.io/docfx/>`_ for the .NET environment
* `Doxygen <https://www.doxygen.nl/index.html>`_, for C/C++ and other languages
* `protoc-gen-doc <https://pkg.go.dev/github.com/pseudomuto/protoc-gen-doc>`_ for Protocol Buffer based APIs

All of the previous examples extract the documentation from source code comments.

* `OpenAPI <https://learn.openapis.org/>`_ Specification (OAS) for REST style APIs, which is provided in one of more YAML or JSON files.
  It is also possible to extract an OAS document from comments in the API source code.

It's not possible to discuss all the different options, so this article focuses on the Open API Specification,
because it is so widely used. An example OAS `specification <_static/openapi.yaml>`_ is used to show some of the integration options.

It is worth noting the OAS document has multiple uses:

* It is used as technical document during the design process
* It can be used to create code from the specification for both server and a client SDK in different languages
* Creating detailed API reference documentation, as discussed here.

----------------------
OpenAPI Specifications
----------------------

Presenting `OpenAPI Specification (OAS) <https://learn.openapis.org/specification/>`_
content in an existing docs website can take some effort.

The OpenAPI Description is written in a YAML or JSON format spread across one or more files.
These file needs to be rendered into a set of resources that can be displayed in a web browser.

Note that OpenAPI documents are not designed to be presented in a static page (for example a PDF) but
some tools are available to create PDF based content. A web search can help you find them.

.. note::

    This document does not explain what, or how to use, an OAS document.
    It assumes that you already have a OAS YAML or JSON file that you need to integrate into your existing docs.

.. vale Vale.Terms = NO

Popular tools to render OAS descriptions include:

* `OpenAPI Generator <https://openapi-generator.tech/>`_,
* `Redoc <https://github.com/Redocly/redoc>`_, or
* `Swagger UI <https://swagger.io/tools/swagger-ui/>`_.

However generally they create standalone content that is not designed to
be integrated into an existing website.

.. vale Vale.Terms = YES

This can present some challenges for site navigation (for example, the loss of breadcrumbs and other navigational cues)
and may make the reader experience jarring as the theme changes.

To illustrate this we are going to look at how two different tools present the example OAS YAML `content <_static/openapi.yaml>`_.
As the content is managed by Sphinx they are both specific to Sphinx.

1. Using the :ref:`Sphinx OpenAPI extension`
============================================

2. Using the :ref:`Sphinx Redoc extension`
==========================================

If neither of these examples are relevant for your project, then a number of other solutions exist (see above or search on the web).
