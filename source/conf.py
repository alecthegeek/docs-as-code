# Configuration file for the Sphinx documentation builder.
#
# This file only contains a selection of the most common options. For a full
# list see the documentation:
# http://www.sphinx-doc.org/en/master/config

# -- Path setup --------------------------------------------------------------

# If extensions (or modules to document with autodoc) are in another directory,
# add these directories to sys.path here. If the directory is relative to the
# documentation root, use os.path.abspath to make it absolute, like shown here.
#
# import os
# import sys
# sys.path.insert(0, os.path.abspath('.'))


# -- Project information -----------------------------------------------------

project = 'Docs Like Code Demo'
copyright = '2019-2021, PaperCut Software International, 2022-2023, Alec Clews'
author = 'Alec Clews'

# The full version, including alpha/beta/rc tags
release = '0.1'


# -- General configuration ---------------------------------------------------

# Add any Sphinx extension module names here, as strings. They can be
# extensions coming with Sphinx (named 'sphinx.ext.*') or your custom
# ones.
extensions = [
    "sphinxcontrib.spelling",
    "sphinxcontrib.openapi",
    "sphinxcontrib.redoc",
    "sphinx.ext.autosectionlabel",
]

# Add any paths that contain templates here, relative to this directory.
templates_path = ['_templates']

# List of patterns, relative to source directory, that match files and
# directories to ignore when looking for source files.
# This pattern also affects html_static_path and html_extra_path.
exclude_patterns = []

# html_extra_path = ["openapi.yaml"]

# -- Options for HTML output -------------------------------------------------

# The theme to use for HTML and HTML Help pages.  See the documentation for
# a list of builtin themes.
#
html_theme = 'alabaster'
# html_theme = 'nature'

html_sidebars = {
    '**': [
         'about.html',
         'navigation.html',
         'relations.html',
         'searchbox.html',
     ]
}

html_theme_options = {
    'logo': 'document.png',
    'description': 'Image created by kerismaker - Flaticon',
    'extra_nav_links': {
        
        'Suggest a Change':
        'mailto:incoming+alecthegeek-docs-as-code-45191553---s_-cULXs6M9M6H-1Bu-issue@incoming.gitlab.com',

        'Download PDF Version': 'docslikecodedemo.pdf',
        'Source repository for these docs': 'https://gitlab.com/alecthegeek/docs-as-code/'},
}

# Add any paths that contain custom static files (such as style sheets) here,
# relative to this directory. They are copied after the builtin static files,
# so a file named "default.css" will overwrite the builtin "default.css".
html_static_path = ['_static']


# -- Options for linkcheck

linkcheck_allowed_redirects = {r' https://www.sphinx-doc\.org/' : r'https://sphinx-doc\.org/en/master/.*'}

# -- Options for spelling

spelling_lang='en_GB'

spelling_word_list_filename="../.styles/Vocab/Base/accept.txt"

#  -- Options for international support

locale_dirs = ['locale/']
gettext_compact = False


#  -- Options ofr Sphinx Reddoc


redoc = [
    {
        'name': 'People API',
        'page': 'SphinxRedoc',
        'spec': 'openapi.yaml',
        'embed': True,
    },
    #{
    #    'name': 'Example API',
    #    'page': 'SphinxRedoc',
    #    'spec': 'http://example.com/openapi.yml',
    #    'opts': {
    #        'lazy': False,
    #        'nowarnings': False,
    #        'nohostname': False,
    #        'required-props-first': True,
    #        'expand-responses': ["200", "201"],
    #    }
    #},
]