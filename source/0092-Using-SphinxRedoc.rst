:orphan: 
.. index::
    single: Sphinx Redoc

======================
Sphinx Redoc extension
======================

The Sphinx static site generator has a contributed extension, `Sphinx Redoc <https://sphinxcontrib-redoc.readthedocs.io/en/stable/>`_.
It displays API schemas in a separate web page and so the user experience can be jarring. However as it uses the Redoc
renderer the content is fully featured.


You can find the OpenAPI specification rendered separately on this `page <_static/SphinxRedoc.html>`_
