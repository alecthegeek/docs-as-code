.. index::
    single: Static Websites
    single: Online Publication


===================
Static Websites
===================
.. contents:: What's Here
    :depth: 3
    :local:

Many documentarians prefer to use static site generators to deploy
websites rather than using a content management system. For documentation
that contains little or no dynamic content they have a number of
advantages

.. vale alex.Condescending = NO

*   Simple infrastructure
*   Easy to use
*   Displays quickly in the user's browser

.. vale alex.Condescending = YES

Examples of static site generators include

*   `Jekyll <https://jekyllrb.com/>`_
*   `Sphinx <https://www.sphinx-doc.org/>`_
*   `Hugo <https://gohugo.io/>`_

Static websites can still contain Javascript, images, and css so the documentation can still be rich. For example the Swagger UI 

-----------------
How Do they Work?
-----------------

A static site generator (SSG) takes as input the various source files needed to create the final content. For example:

* Plain text markup files
* Images and image descriptions
* Styling information (for example CSS files)
* Javascript files needed make pages function
* Other data, such as table of contents descriptions

and creates the final content as a set of HTML files, images, and other assets that can be delivered by a web server and displayed in a web browser.

The files (the website) can be transmitted to the browser via a web server, or read directly from local file storage for display.
The latter is useful to preview change locally before they are deployed to a web server for general consumption.

.. image:: images/ssg-workflow.png


See the :doc:`1000-further-information` section for a presentation about static site generators.

The following two sections provide a practical example of how to create and deploy a basic
static website using Hugo and `GitLab Pages <https://about.gitlab.com/stages-devops-lifecycle/pages/>`_.

The example Git repository for this example project can be found in the GitLab project
`demo-docs-project <https://gitlab.com/papercut-docs-as-code/demo-docs-project>`_.

---------------------------------
Setting up a small Hugo website
---------------------------------

Hugo has become a popular choice for many static websites because of its speed, power, and flexibility.
However, it can daunting for the first time user. These notes should help you create your first basic Hugo project.

#. Install Git if needed (for example `on Windows <https://papercutsoftware.github.io/git-from-powershell/#installing-and-configuring-git-on-windows>`_)
#. `Install Hugo locally <https://gohugo.io/installation/>`_ [#]_
#. Create a new website project in it's own directory

   .. code-block:: bash

      hugo new site docsite
   ..

#. Setup source control and check the configuration

   .. code-block:: bash

      cd docsite
      git init
      # Make sure version control is correctly configured
      git config --get user.name
      git config --get user.email
   ..

#. Install a Hugo theme

   * Find Hugo themes tagged with "documentation" on the `themes <https://themes.gohugo.io/tags/docs/>`_ website
   * Each theme will have specific setup and configuration requirements so be sure to read the theme docs. For example

   .. code-block:: bash

      mkdir -p themes/hugo-geekdoc/ && \
      curl -L https://github.com/thegeeklab/hugo-geekdoc/releases/latest/download/hugo-geekdoc.tar.gz |
      tar -xz -C themes/hugo-geekdoc/ --strip-components=1
   ..

#. Configure the theme in the top level configuration file ``config.toml``.
   For example  ``theme="hugo-geekdoc"``

   * Perform any additional theme specific setup required. Consult the theme documentation for details

#. With the configuration file open setup any other values (for example title). Hint: use ``baseURL = ""``
 
#. Create a top level page with ``hugo new _index.md``.
   This creates an (almost) empty file called ``content/_index.md``
   that will become the top level ``index.html`` asset in your final website.
   Note that the website content is located inside the `content` directory

#. Edit the new ``content/_index.md`` and add new content.
   Notice the top level front matter that Hugo added when it created the file.
   All content files need this front matter.
 

#. Preview your new single page website. This requires two steps:

   #. Generate the website static files

   #. Start a local web server to preview local files.

   Hugo has an option to do both of these steps with a single command


   .. code-block:: bash

      hugo -D serve --disableFastRender --bind "0.0.0.0"
   ..

   * ``-D``   Generate pages that are marked draft
   * ``serve`` Start the Hugo web server
   * ``--disableFastRender``     Every time a change is made to the content or settings reload it
   * ``--bind "0.0.0.0"``  Listen on every network interface, so I can test on my cell phone (default is to only respond to requests from `localhost`)

   More information on command line options in the Hugo `docs <https://gohugo.io/commands/hugo/>`_.

   Note: In ``server`` mode Hugo does not write the static files to disk -- it serves content from memory. In order to create the files in ``public`` directory we
   can serve from any web server either

   * Don't use the ``serve`` command -- this is the option we will generally use (see next step)
   * provide a ``--contentDir public`` option (``-d public`` also works)


#. Now add more content pages. For example,  ``hugo new Getting-Started/_index``...

   I recommend organising content as `page bundles <https://gohugo.io/content-management/organization/#page-bundles>`_ in the first instance

.. <!--alex ignore just-->

   Note: You can copy in existing Markdown content, just be sure to add front matter at the top of each file.

   As a slightly different approach let's review this using the Nginx web server (you will need
   `Docker <https://www.docker.com/products/docker-desktop>`_ installed on your local machine).

   .. code-block:: bash

      hugo -D
      docker run --name mynginx -v $PWD/public:/usr/share/nginx/html:ro -it --rm -p 8080:80  nginx"
   ..

   If you don't have Docker installed then you can use this command instead

   .. code-block:: bash

      hugo -D serve --disableFastRender --contentDir public
   ..

   It's worth reviewing the contents of the ``public`` directory to see the website files that Hugo has generated.

You have now set up your first local Hugo website


----------------------------------------
Deploying Hugo content with GitLab pages
----------------------------------------

We will continue the basic example above and use  GitLab's Continuous Delivery feature
(refereed to as `CI/CD <https://about.gitlab.com/topics/ci-cd/>`_) to generate content which we
published on GitLab `pages <https://papercut-docs-as-code.gitlab.io/demo-docs-project/>`_.

This very basic workflow (for example, no formal reviews and all work is done on the ``main`` branch) works as follows:

#. A writer makes a local change to the docs on their workstation
#. Review the change on the workstation with a local Hugo build (see above)
#. Change is committed to version control using Git
#. The commit is pushed to the GitLab cloud service
#. GitLab kicks of a job generate website content
#. At the end of the job GitLab deploys any content in the ``/public`` directory the project's GitLab Pages website.

In order for this to work some setup is required.

#. Create a free personal account on `GitLab  <https://gitlab.com/users/sign_up>`_
#. Create a new project (Git repository) on the GitLab website

   .. image:: images/GitLabCreateProject.png

#. Avoid adding the theme and the generated content to the version control by adding a ``.gitignore`` file with the contents

   .. code-block::

      /public
      /themes
   ..

#. Link the local Git repository with the GitLab repo. For example:

   .. code-block:: bash

      git remote add origin git@gitlab.com:alecthegeek/myapidocs.git
      git add .
      git commit -m "Initial commit"
      git push -u origin main # Note, my default Git branch is main
   ..

#. Review the repository contents on the GitLab website.
   **Our project is now under version and backed up to a cloud service.**

.. <!-- alex ignore special -->

#. In order to configure the creation of the Pages website we need to a add a special CI file called ``.gitlab-ci.yml``.

   The ``.gitlab-ci.yml`` file specifies the actions needed to generate our website content.
   The actual content of the fie can look arcane, but in principle it's quite simple.:

   #. Specify a Hugo specific environment and version in which the generation can take place

      .. code-block::

         image: registry.gitlab.com/pages/hugo/hugo_extended:0.82.1
      ..

   #. Under the ``script`` directive install the required theme (note that we install a specific version rather than ``latest``)

      .. code-block::

         # Install the Hugo Geekdoc theme at version 0.11.1
         - mkdir -p themes/hugo-geekdoc/ &&
             wget -O - https://github.com/thegeeklab/hugo-geekdoc/releases/download/v0.11.1/hugo-geekdoc.tar.gz |
             tar -xzf - -C themes/hugo-geekdoc/
      ..

   #. Run a script command to edit the ``config.toml`` file as the ``baseURL`` setting has to be modified for GitLab Pages style URLs.

      .. code-block::

         # Configure the Hugo website with the correct public URL
         - sed -i -e '\|^.*baseURL.*=.*$|s||baseURL = "'${CI_PAGES_URL}'"|' config.toml
      ..

   #. Finally run ``hugo`` to create the website content in the ``public`` directory.

      .. code-block::

         # Generate the website assets
         - hugo --verbose -D
      ..

   #. Finally after the ``srcipt`` section has run the generated content in ``public`` is saved

      .. code-block::

         # Preserve the generated content in ./public
         artifacts:
           paths:
           - public
      ..

#. Add the file to the repository, commit the change and push

   You can find a complete example ``.gitlab-ci.yml`` file `here <https://gitlab.com/alecthegeek/docs-as-code/-/blob/main/.gitlab-ci.yml>`_

   .. code-block::

      git add .gitlab-ci.yml
      git commit -m "Added CI file for Pages"
      git push
   ..

#. Check on the progress of the Pages job in the GitLab web interface.

   .. image:: images/pages-jobs.png

#. Review the website content on the project Pages website.

   .. image:: images/pages-website.png

.. rubric:: Footnotes

.. [#] Docker is also an option
