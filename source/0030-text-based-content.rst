.. index::
    single: Plain Text Markup
    single: Restructured Text
    single: RST
    single: Markdown
    single: AsciiDoc

=================
Plain Text Markup
=================

.. contents:: What's Here
    :depth: 3
    :local:


If written content is edited and maintained in plain text files there are a number of advantages

1. Content can edited in a wide range of tools (text editors),
for instance by developers using their existing development environments
2. It is easier to store the content in a version control tool

However, there are limitations in using pure plain text content. For example

1. No support for rich formatting. (headers, lists, blocks, styles, ...)
2. Cross references
3. Creating the index, table of contents and bibliography

Fortunately there are a number lightweight text based markup languages that allow us to
embed tags into our content to provide (most) of the features we need.
However, sometimes compromises have to be made as many of these text based markup
system do have some limitations (for example Markdown does not support numbered sections).

Examples include

* Markdown (for example `GitHub flavoured Markdown <https://github.github.com/gfm/#what-is-github-flavored-markdown->`_)

* `Sphinx <https://www.sphinx-doc.org/>`_
  using `reStructuredText <https://docutils.sourceforge.io/rst.html>`_
  (this content is written in reStructuredText and published with Sphinx)

* `AsciiDoc <https://asciidoctor.org/>`_

--------------------------------------
Lightweight Markup compared to DocBook
-------------------------------------- 

For the ultimate in text markup systems then options like
`DocBook XML <https://docbook.org/>`_ might appear attractive
and will give you all the features
you need.
However there is a corresponding complexity and the XML
markup in DocBook can be difficult to edit in a text editor.
This approach is not recommended.

**Top Tip**: The `Pandoc <https://pandoc.org/>`_
text processing tool is invaluable for migrating
various text (and a few proprietary) document formats.

.. <!--alex ignore simple-->

For example, consider some basic Markdown text (a header and an included image)

.. literalinclude:: example_content.md

We can translate into XML DocBook as follows:

.. code-block:: bash

    pandoc -t docbook -o sample_file.dbx -f gfm sample_file.md 

and get the DocBook equivalent for use a large publication system

.. literalinclude:: example_content.dbx
    :language: XML

.. spelling:word-list::

This process can be represented as follows

.. figure:: images/markdown2docbook.png
   :alt: The flow of data through the Pandoc utility
    
