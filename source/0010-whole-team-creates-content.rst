===========================
Whole Team Approach
===========================
.. contents:: What's Here
    :depth: 3
    :local:

.. vale Google.Quotes = NO

It has been a popular mantra in the technical documentation community for decades
that "everyone is responsible for the creation of technical documentation".
However, the reality is often very different, with the documentation locked
away and the technical writers the only people able to make changes or
create new content.

.. vale Google.Quotes = YES

Content creation often remains the
burden of the technical writer for a variety of reason.
Each organisation is different, but common examples includes:

* Political inertia, and *Adopting new tools and processes won't fix that*.
* The engineering team may be unwilling to take on extra writing tasks and do not see it
  natural part of the development process.

But if you can fix (or start to fix) the expectations and culture
around this then the technical writer can be potentially be freed
to :doc:`deliver higher value </0070-role-of-the-tech-writer>`.

In order to engage product developers in the writing process and
get their support they must be persuaded that their time and
energy will not be wasted.

Documentation that is written for contractual or legal
purposes only will probably need to be created and maintained
by the technical writer.

However the product engineers and developers can be (or should be)
expected to create the product documentation content.

This expectation should be aligned with the creation of code,
design documentation, test artifacts etc. The job is not
complete until the documentation has been updated and reviewed
to meet the following criteria.

.. vale alex.Condescending = NO

* Useful
* Easy to Use
* Accurate
* Up to date

.. vale alex.Condescending = YES

With appropriate training expect senior development staff to
take greater responsibility for the quality of the documentation.

Finally technical writing tools may only
be available to a select few in the writing team. Docs-As-Code fixes that.
