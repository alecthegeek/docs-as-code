.. index::
    single: Introduction
    
============
Introduction
============
.. contents:: What's Here
    :depth: 3
    :local:

Acknowledgement: This work was originally sponsored by `PaperCut Software  <https://www.papercut.com>`_

When projects grow documentation is often managed by dedicated teams of technical writers to improve scalability and quality.
They bring valuable skills and experience, plus their own tools and processes.

However frequently the result is that content can only be managed with hard to use (and sometimes expensive)
authoring tools,
text is stored in proprietary binary formats; and only the technical writers can make even trivial changes

Not getting the whole team involved means we lose contributions and the perspectives of other team members.
Developers  also learn less about the customer's experience of their product.

.. <!-- alex ignore mad-->

At some point publication can become slow and cumbersome,
and it can be a mad scramble to get the documentation ready for each release

**Docs become the bottleneck in many projects**

.. vale Google.Quotes = NO

Docs-As-Code, also known as "Docs-Like-Code",
has become a popular way to get the whole team to deliver documentation quickly as part of the development process.

.. vale Google.Quotes = YES

The phrase is open to interpretation, but there are two common themes
that you can expect to see.

* Adopt an ":term:`agile`" approach to content creation, namely
    #. The whole team is responsible for content
    #. Be adaptive and improve both your content, and your process, over time

* Use developer tools, and processes, to create and deliver content. Specifically:
    #. Text based file content with embedded, `lightweight`, markup tags
    #. Developer based workflows. For example:
        #. Version Control using tools such as Git
        #. Change control driven though bugs and feature requests tickets
        #. Content reviews and merges
    #. Machine generated content
    #. Automated testing and verification
    #. Build tools to make it convenient and fast to use complex tool chains
    #. Rapid online publication using web pages (using static site generators) or wikis.
       For example `Sphinx <https://www.sphinx-doc.org/>`_ or `Hugo <https://gohugo.io/>`_.

In summary Docs As Code is adoption of current software development and ":term:`DevOps`" practices into the documentation lifecycle.
Both front end (edit and reviewing content) as well a back end publication.

* The whole team contributes

* Creating useful content ("...repeatable, consistent collaboration…." Collaborative authoring)

* That is always current

* Whilst proactively improving

* Across the whole writing and publishing process

* "Keep the docs close to the code"

    (but does not have to be in the same Git repo)

"We treat infrastructure as code, why not Docs?"

Benefits Can Include

* Faster Publication Cycles
* Reduced load on the technical writing team to create content
* Reduced costs for monolithic authoring and publication tools
* Reduced training requirements


Docs-as-Code is still an evolving approach and you will see many variations
on these ideas. Tools and processes will need to be adapted
for your project (and then adapted again for the project after that).

Hopefully this material will provide a set of useful ideas, patterns, terminology,
tools and processes for your documentation toolkit.

.. marker1

----------------
Example Workflow
----------------

.. image:: images/workflow.png

.. marker2

--------
See also
--------
`...what we mean by docs like code <https://idratherbewriting.com/2017/06/02/when-docs-are-not-like-code/#first-what-we-mean-by-docs-like-code>`_
by Tom Johnson.
